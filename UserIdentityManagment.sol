pragma solidity ^0.4.10;

import "./UserIdendity";
import "./AuthManager";
import "./OnboardManager";
import "./SecurityManager";

contract UserIdentityManagment is mortal{

    UserIndendity userIndendity;
    AuthManager authManager;
    OnboardManager onboardManager;
    SecurityManager securityManager;
    address[]  authManagerContractList;
    address[]  onboardManagerContractList;
    address[]  securityManagerContractList;
    mapping (address=>address)  UserInfo;

    function UserIdentityManagment(){
        owner=msg.sender;
    }

    function addUser(address _userAddress,address _contractAddress) isOwner returns(bool){
        if(!checkUserExist(_userAddress)){
            UserInfo[_userAddress]=_contractAddress;
            return true;
        }
        return false;
    }

    function addAuthManagerContract(address _authManagerContractAddress) isOwner returns(bool){
        authManagerContractList.push(_authManagerContractAddress);
        return true;
    }

    function getAuthManagerContract() private returns(address){
        return authManagerContractList[authManagerContractList.length-1];
    }

    function addOnboardManagerContract(address _onboardManagerContractAddress) isOwner returns(bool){
        onboardManagerContractList.push(_onboardManagerContractAddress);
        return true;
    }

    function getOnboardManagerContract() private returns(address){
        return onboardManagerContractList[onboardManagerContractList.length-1];
    }

    function addSecurityManagerContract(address _securityManagerContractAddress) isOwner returns(bool){
        securityManagerContractList.push(_securityManagerContractAddress);
        return true;
    }

    function getSecurityManagerContract() private returns(address){
        return securityManagerContractList[securityManagerContractList.length-1];
    }

    function verifyUser(address _userAddress) isAuthManager returns (bool){
        address currentUser=msg.sender;
        if(checkUserExist(currentUser) && checkUserExist(_userAddress)){
            userIndendity=UserIndendity(UserInfo[_userAddress]);
            return userIndendity.verifyUser(_userAddress);
        }
        return false;
    }

    function checkUserExist(address _userAddress) constant private returns(bool){
        address contractAddress=UserInfo[_userAddress];
        if(contractAddress != address(0)){
            return true;
        }else{
            return false;
        }
    }

    function getUserDetail(address _userAddress) isOnboardManager constant external returns (address,address,bytes32,bytes32,address,bytes32,uint64,bytes32,bytes32,bytes32,bytes32,uint8) {
        if(checkUserExist(_userAddress)){
            userIndendity=UserIndendity(UserInfo[_userAddress]);
            return userIndendity.UserDetail(_userAddress);
        }
    }

    modifier isAuthManager(){
        authManager=AuthManager(getAuthManagerContract());
        if(msg.sender != authManager.getAuthManager()){
            throw;
        }else{
            _;
        }
    }

    modifier isOnboardManager(){
        onboardManager=OnboardManager(getOnboardManagerContract());
        securityManager=SecurityManager(getSecurityManagerContract());
        if(msg.sender != onboardManager.getOnboardManager() || msg.sender != securityManager.getSecurityManager()){
            throw;
        }else{
            _;
        }
    }

}