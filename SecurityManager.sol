pragma solidity ^0.4.0;

import "./Mortal";

contract SecurityManager is mortal{

    address securityManagerAddress;

    function SecurityManager(address _securityManagerAddress){
        owner=msg.sender;
        securityManagerAddress=_securityManagerAddress;
    }

    function updateSecurityManager(address _securityManagerAddress) isOwner{
        securityManagerAddress=_securityManagerAddress;
    }

    function getSecurityManager() returns(address){
        return securityManagerAddress;
    }

}