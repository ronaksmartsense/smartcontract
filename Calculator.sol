pragma solidity ^0.4.2;

contract AddValue{
    function getAddValue(uint a,uint b) returns (uint){
        return a+b;
    }
}

contract MultiValue{
    function getMultiValue(uint a,uint b) returns (uint){
        return a*b;
    }
}


contract Calculator{
    AddValue addvalue;
    MultiValue multivalue;
    uint[] public allValues;

    function addValue(address addValueAddress,uint a,uint b){
        addvalue = AddValue(addValueAddress);
        allValues.push(addvalue.getAddValue(a,b));
    }

    function multiValue(address addValueAddress,uint a,uint b){
        multivalue = MultiValue(addValueAddress);
        allValues.push(multivalue.getMultiValue(a,b));
    }

}