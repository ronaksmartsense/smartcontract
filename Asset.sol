pragma solidity ^0.4.2;

contract mortal {
    address owner;

    modifier isOwner(){
        if(msg.sender != owner){
            throw;
        }else{
            _;
        }
    }

    function deleteContract() isOwner{
        suicide(owner);
    }

}

contract will{
    address willManagerAddress;
    modifier isWill(){
        if(msg.sender != willManagerAddress){
            throw;
        }else{
            _;
        }
    }
}

contract Asset is mortal,will{

    address deathCertificateAddress;

    struct NomineeInfo {
    address nomineeAddress;
    bool isValid;
    }

    struct DoumentInfo {
    address assetDoumentsAddress;
    bool isValid;
    }

    struct PriceInfo {
    uint price;
    bool isValid;
    }

    struct AssetInfo {
    string assetName;
    PriceInfo[] priceInfoList;
    bool isOwner;
    address ownerAddress;
    NomineeInfo[] nomineeInfoList;
    DoumentInfo[] doumentInfoList;
    }

// AssetInfo assetInfo;

    mapping(address=>AssetInfo) public userAsset;


    function Asset(string _assetName,uint _price,address _nomineeAddress,address _assetDouments,address _willManagerAddress){

        owner=msg.sender;

        if(_willManagerAddress == address(0)){
            throw;
        }else{
            willManagerAddress=_willManagerAddress;
        }
        AssetInfo assetInfo=userAsset[owner];
        assetInfo.assetName=_assetName;

        assetInfo.ownerAddress=owner;
        assetInfo.isOwner=true;

        PriceInfo memory priceInfo;
        priceInfo.price=_price;
        priceInfo.isValid=true;
        assetInfo.priceInfoList.push(priceInfo);

        NomineeInfo memory nomineeInfo;
        nomineeInfo.nomineeAddress=_nomineeAddress;
        nomineeInfo.isValid=true;
        assetInfo.nomineeInfoList.push(nomineeInfo);

        DoumentInfo memory doumentInfo;
        doumentInfo.assetDoumentsAddress=_assetDouments;
        doumentInfo.isValid=true;
        assetInfo.doumentInfoList.push(doumentInfo);

    }

    function transferAsset(address _newOwnerAddress) isOwner returns(bool){
        AssetInfo assetInfoOld=userAsset[owner];
        assetInfoOld.isOwner=false;
        AssetInfo assetInfo=userAsset[_newOwnerAddress];
        assetInfo.assetName=assetInfoOld.assetName;
        assetInfo.priceInfoList=assetInfoOld.priceInfoList;
        assetInfo.ownerAddress=_newOwnerAddress;
        assetInfo.isOwner=true;
        assetInfo.doumentInfoList=assetInfoOld.doumentInfoList;
        owner=_newOwnerAddress;
        return true;
    }

    function buyAsset(address _newOwnerAddress) {
    // if (msg.value < price) {
    //     return false;
    // }
    // if (!owner.send(msg.value) {
    //     throw;
    // }
    // owner=_newOwnerAddress;
    }

    function updateNominee(address _newNomineeAddress) isOwner returns(bool){
        disableAllNominee();
        NomineeInfo memory nomineeInfo;
        nomineeInfo.nomineeAddress=_newNomineeAddress;
        nomineeInfo.isValid=true;
        userAsset[owner].nomineeInfoList.push(nomineeInfo);
        return true;
    }

    function disableAllNominee() private isOwner{
        NomineeInfo[] list=userAsset[owner].nomineeInfoList;
        for(uint i=list.length-1;i>=0;i--){
            list[i].isValid=false;
        }
    }


    function getCurrentNominee() private isOwner returns(address){
        NomineeInfo[] list=userAsset[owner].nomineeInfoList;
        for(uint i=list.length-1;i>=0;i--){
            if(list[i].isValid==true){
                return list[i].nomineeAddress;
            }
        }
        return address(0);
    }

    function addAssetDoument(address _assetDoumentsAddress) isOwner returns(bool){
        DoumentInfo memory doumentInfo;
        doumentInfo.assetDoumentsAddress=_assetDoumentsAddress;
        doumentInfo.isValid=true;
        userAsset[owner].doumentInfoList.push(doumentInfo);
        return true;
    }


    function getAllDocument() isOwner returns(address){
        DoumentInfo[] list=userAsset[owner].doumentInfoList;
        return list[list.length-1].assetDoumentsAddress;
    }

    function updateAssetPrice(uint _assetPrice) isOwner returns(bool){
        disableAllPrice();
        PriceInfo memory priceInfo;
        priceInfo.price=_assetPrice;
        priceInfo.isValid=true;
        userAsset[owner].priceInfoList.push(priceInfo);
        return true;
    }

    function disableAllPrice() private isOwner{
        PriceInfo[] list=userAsset[owner].priceInfoList;
        for(uint i=list.length-1;i>=0;i--){
            list[i].isValid=false;
        }
    }

    function getCurrentPrice() private isOwner returns(uint){
        PriceInfo[] list=userAsset[owner].priceInfoList;
        for(uint i=list.length-1;i>=0;i--){
            if(list[i].isValid==true){
                return list[i].price;
            }
        }
        return 0;
    }


    function addDeathCertificate(address _deathCertificateAddress) isWill returns(bool){
        deathCertificateAddress=_deathCertificateAddress;
        address nomineeAddress=getCurrentNominee();
        if(nomineeAddress == address(0)){
            suicide(willManagerAddress);
        }else{
            owner=nomineeAddress;
        }
        return true;
    }


    function getAssetInfo() constant returns(string, uint, bool,address, address,address) {
        AssetInfo assetInfo=userAsset[owner];
        uint price=getCurrentPrice();
        address nomineeAddress=getCurrentNominee();
        address documentAddress=getAllDocument();
        return (assetInfo.assetName,price,assetInfo.isOwner,assetInfo.ownerAddress,documentAddress,nomineeAddress);
    }

}