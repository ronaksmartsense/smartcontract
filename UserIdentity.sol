pragma solidity ^0.4.2;

import "./Mortal";

contract UserIndendity is mortal{

    struct User {
    address userAccount;
    address photo;
    bytes32 fName;
    bytes32 lName;
    address signature;
    bytes32 email;
    uint64 mno;
    bytes32 street;
    bytes32 city;
    bytes32 state;
    bytes32 country;
    uint8 count;
    address[] verifyBy;
    }

    mapping (address=>User) public UserDetail;

    function UserIndendity(address _userAccount,address _photo,bytes32 _fName,bytes32 _lName,address _signature,bytes32 _email,uint64 _mno,bytes32 _street,bytes32 _city,bytes32 _state,bytes32 _country){
        owner=msg.sender;
        User memory user;
        user.userAccount=_userAccount;
        user.photo=_photo;
        user.fName=_fName;
        user.lName=_lName;
        user.signature=_signature;
        user.email=_email;
        user.mno=_mno;
        user.street=_street;
        user.city=_city;
        user.state=_state;
        user.country=_country;
        UserDetail[_userAccount]=user;
    }

    function verifyUser(address _userAccount) returns (bool){
        address currentUser=msg.sender;
        if(checkAlreadyVerified(currentUser,_userAccount)){
            UserDetail[_userAccount].count=UserDetail[_userAccount].count+1;
            UserDetail[_userAccount].verifyBy.push(currentUser);
            return true;
        }
        return false;
    }


    function checkAlreadyVerified(address currentUser,address _userAccount) private returns(bool){
        for(uint i=0;i<UserDetail[_userAccount].verifyBy.length;i++){
            if(UserDetail[_userAccount].verifyBy[i]==currentUser){
                return false;
            }
        }
        return true;
    }

    function getVerifiedList(address _userAccount) returns (address[]){
        return UserDetail[_userAccount].verifyBy;
    }

}