pragma solidity ^0.4.0;

import "./Mortal";

contract OnboardManager is mortal{

    address onBoardManagerAddress;

    function OnboardManager(address _onBoardManagerAddress){
        owner=msg.sender;
        onBoardManagerAddress=_onBoardManagerAddress;
    }

    function updateOnboardManager(address _onBoardManagerAddress) isOwner{
        onBoardManagerAddress=_onBoardManagerAddress;
    }

    function getOnboardManager() returns(address){
        return onBoardManagerAddress;
    }

}