pragma solidity ^0.4.0;

import "./Mortal";

contract AuthManager is mortal{

    address authManagerAddress;

    function AuthManager(address _authManagerAddress){
        owner=msg.sender;
        authManagerAddress=_authManagerAddress;
    }

    function updateAuthManager(address _authManagerAddress) isOwner{
        authManagerAddress=_authManagerAddress;
    }

    function getAuthManager() returns(address){
        return authManagerAddress;
    }

}