pragma solidity ^0.4.10;

contract mortal{
    address owner;

    modifier isOwner(){
        if(msg.sender != owner){
            throw;
        }else{
            _;
        }
    }

    function deleteContract() isOwner{
        suicide(owner);
    }

}

contract AssetManagment is mortal{

    struct User {
    address userAccount;
    string name;
    address[] assetList;
    mapping (address => address[])  assetOwnerList;
    }

    User[] users;

    function AssetManagment(){
        owner=msg.sender;
    }

    function addUser(string _name,address _userAddress) isOwner{
        User memory u;
        u.userAccount=_userAddress;
        u.name=_name;
        users.push(u);
    }

    function addAsset(address _assetAddress) {
        uint userIndex=isUser(msg.sender);
        if( userIndex != 0 && isAsset(userIndex,_assetAddress) == 0){
            users[userIndex-1].assetList.push(_assetAddress);
            users[userIndex-1].assetOwnerList[_assetAddress].push(msg.sender);
        }else{
            throw;
        }
    }

    function transferAsset(address _assetAddress,address _newAssetOwnder) {
        uint assetOwnderIndex=isUser(msg.sender);
        uint newAssetOwnderIndex=isUser(_newAssetOwnder);
        uint assetIndex=isAsset(assetOwnderIndex,_assetAddress);
        if(assetOwnderIndex!=0 && newAssetOwnderIndex!=0 && assetIndex!=0){
            delete users[assetOwnderIndex-1].assetList[assetIndex];
            delete users[assetOwnderIndex-1].assetOwnerList[_assetAddress];
            users[newAssetOwnderIndex-1].assetList.push(_assetAddress);
            users[newAssetOwnderIndex-1].assetOwnerList[_assetAddress].push(msg.sender);
        }else{
            throw;
        }
    }

    function isUser(address _userAddress) returns(uint) {
        for (uint i = 0; i < (users).length; i++) {
            if (users[i].userAccount == _userAddress) {
                return (i+1);
            }
        }
        return 0;
    }

    function isAsset(uint _userIndex,address _assetAddress) returns(uint) {
        for (uint i = 0; i < (users[_userIndex].assetList).length; i++) {
            if (users[_userIndex].assetList[i] == _assetAddress) {
                return (i+1);
            }
        }
        return 0;
    }

}