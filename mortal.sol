pragma solidity ^0.4.0;
contract mortal {
    address public owner;

    modifier isOwner(){
        if(msg.sender != owner){
            throw;
        }else{
            _;
        }
    }

    function deleteContract() isOwner{
        suicide(owner);
    }
}