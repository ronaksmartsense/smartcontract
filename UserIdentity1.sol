pragma solidity ^0.4.2;

import "./Mortal";

contract UserIndendity is mortal{

    mapping (address=>address[]) photoVerify;
    mapping (bytes32=>address[]) fNameVerify;
    mapping (bytes32=>address[]) lNameVerify;
    mapping (address=>address[]) signatureVerify;
    mapping (bytes32=>address[]) emailVerify;
    mapping (uint64=>address[]) mnoVerify;
    mapping (bytes32=>address[]) streetVerify;
    mapping (bytes32=>address[]) cityVerify;
    mapping (bytes32=>address[]) stateVerify;
    mapping (bytes32=>address[]) countryVerify;

    struct User {
    address[] userAccount;
    address[] photo;
    bytes32[] fName;
    bytes32[] lName;
    address[] signature;
    bytes32[] email;
    uint64[] mno;
    bytes32[] street;
    bytes32[] city;
    bytes32[] state;
    bytes32[] country;
    }

    User user;

    function UserIndendity(address _userAccount,address _photo,bytes32 _fName,bytes32 _lName,address _signature,bytes32 _email,uint64 _mno,bytes32 _street,bytes32 _city,bytes32 _state,bytes32 _country){
        owner=msg.sender;
        user.userAccount.push(_userAccount);
        user.photo.push(_photo);
        user.fName.push(_fName);
        user.lName.push(_lName);
        user.signature.push(_signature);
        user.email.push(_email);
        user.mno.push(_mno);
        user.street.push(_street);
        user.city.push(_city);
        user.state.push(_state);
        user.country.push(_country);
    }

    function getUserDetail() returns(address _userAccount,address _photo,bytes32 _fName,bytes32 _lName,address _signature,bytes32 _email,uint64 _mno,bytes32 _street,bytes32 _city,bytes32 _state,bytes32 _country){
        return (getUserAccount(0,true),getUserPhoto(0,true),getUserFName(0,true),getUserLName(0,true),getUserSignature(0,true),getUserEmail(0,true),getUserMno(0,true),getUserStreet(0,true),getUserCity(0,true),getUserState(0,true),getUserCountry(0,true));
    }

    function getUserAccount(uint index,bool check) private returns(address){
        if(check){
            return user.userAccount[user.userAccount.length-1];
        }else{
            return user.userAccount[index];
        }
    }

    function getUserPhoto(uint index,bool check) private returns(address){
        if(check){
            return user.photo[user.photo.length-1];
        }else{
            return user.photo[index];
        }
    }

    function getUserFName(uint index,bool check) private returns(bytes32){
        if(check){
            return user.fName[user.fName.length-1];
        }else{
            return user.fName[index];
        }
    }

    function getUserLName(uint index,bool check) private returns(bytes32){
        if(check){
            return user.lName[user.lName.length-1];
        }else{
            return user.lName[index];
        }
    }

    function getUserSignature(uint index,bool check) private returns(address){
        if(check){
            return user.signature[user.signature.length-1];
        }else{
            return user.signature[index];
        }
    }

    function getUserEmail(uint index,bool check) private returns(bytes32){
        if(check){
            return user.email[user.email.length-1];
        }else{
            return user.email[index];
        }
    }

    function getUserMno(uint index,bool check) private returns(uint64){
        if(check){
            return user.mno[user.mno.length-1];
        }else{
            return user.mno[index];
        }
    }

    function getUserStreet(uint index,bool check) private returns(bytes32){
        if(check){
            return user.street[user.street.length-1];
        }else{
            return user.street[index];
        }
    }

    function getUserCity(uint index,bool check) private returns(bytes32){
        if(check){
            return user.city[user.city.length-1];
        }else{
            return user.city[index];
        }
    }

    function getUserState(uint index,bool check) private returns(bytes32){
        if(check){
            return user.state[user.state.length-1];
        }else{
            return user.state[index];
        }
    }

    function getUserCountry(uint index,bool check) private returns(bytes32){
        if(check){
            return user.country[user.country.length-1];
        }else{
            return user.country[index];
        }
    }

    function updateDetails(address _userAccount,address _photo,bytes32 _fName,bytes32 _lName,address _signature,bytes32 _email,uint64 _mno,bytes32 _street,bytes32 _city,bytes32 _state,bytes32 _country) returns(bool){
        if(_userAccount != address(0))
        user.userAccount.push(_userAccount);
        if(_photo != address(0))
        user.photo.push(_photo);
        if(_fName != bytes32(0))
        user.fName.push(_fName);
        if(_lName != bytes32(0))
        user.lName.push(_lName);
        if(_signature != address(0))
        user.signature.push(_signature);
        if(_email != bytes32(0))
        user.email.push(_email);
        if(_mno != uint32(0))
        user.mno.push(_mno);
        if(_street != bytes32(0))
        user.street.push(_street);
        if(_city != bytes32(0))
        user.city.push(_city);
        if(_state != bytes32(0))
        user.state.push(_state);
        if(_country != bytes32(0))
        user.country.push(_country);
        return true;
    }

    function aadUserDetailVerify(address verifyByAccount,address _userAccount,address _photo,bytes32 _fName,bytes32 _lName,address _signature,bytes32 _email,uint64 _mno,bytes32 _street,bytes32 _city,bytes32 _state,bytes32 _country) returns (bool){
        if(_photo != address(0))
        photoVerify[_photo].push(verifyByAccount);
        if(_fName != bytes32(0))
        fNameVerify[_fName].push(verifyByAccount);
        if(_lName != bytes32(0))
        lNameVerify[_lName].push(verifyByAccount);
        if(_signature != address(0))
        signatureVerify[_signature].push(verifyByAccount);
        if(_email != bytes32(0))
        emailVerify[_email].push(verifyByAccount);
        if(_mno != uint32(0))
        mnoVerify[_mno].push(verifyByAccount);
        if(_street != bytes32(0))
        streetVerify[_street].push(verifyByAccount);
        if(_city != bytes32(0))
        cityVerify[_city].push(verifyByAccount);
        if(_state != bytes32(0))
        stateVerify[_state].push(verifyByAccount);
        if(_country != bytes32(0))
        countryVerify[_country].push(verifyByAccount);
        return true;
    }

    function getUserPhotoVerifyList() returns (address[]){
    // retrun photoVerify[getUserPhoto(0,true)];
    }


    function getUserPhotoVerifyList1(string _str) returns (uint32){
        return _str.length;
    }
}